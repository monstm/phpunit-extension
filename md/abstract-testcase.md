# Abstract TestCase

This is a simple TestCase implementation that other TestCase can inherit from.

---

## TestCase Instance

Simple TestCase Implementation.

```php
class MyTest extends \Samy\PhpUnit\AbstractTestCase
{
    // ...
}
```

---

## Print console

### printOut - protected

Print out message to console.

```php
$self = $this->printOut($message);
```

### printError - protected

Print error message to console.

```php
$self = $this->printError($message);
```

---

## Load file data

### csv - protected

Retrieve csv data.

```php
$csv = $this->csv($filename);
```

### lst - protected

Retrieve lst data.

```php
$lst = $this->lst($filename);
```

### json - protected

Retrieve json data.

```php
$json = $this->json($filename);
```
