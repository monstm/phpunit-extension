# PHPUnit Extension

[
	![](https://badgen.net/packagist/v/samy/phpunit/latest)
	![](https://badgen.net/packagist/license/samy/phpunit)
	![](https://badgen.net/packagist/dt/samy/phpunit)
	![](https://badgen.net/packagist/favers/samy/phpunit)
](https://packagist.org/packages/samy/phpunit)

PHPUnit Extension.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require --dev samy/phpunit
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/phpunit-extension>
* User Manual: <https://monstm.gitlab.io/phpunit-extension/>
* Documentation: <https://monstm.alwaysdata.net/phpunit-extension/>
* Issues: <https://gitlab.com/monstm/phpunit-extension/-/issues>
