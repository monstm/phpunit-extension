# Abstract DataProvider

This is a simple DataProvider implementation that other DataProvider can inherit from.

---

## DataProvider Instance

Simple DataProvider Implementation.

```php
class MyDataProvider extends \Samy\PhpUnit\AbstractDataProvider
{
    // ...
}
```

---

## Print console

### printOut - protected

Print out message to console.

```php
self::printOut($message);
```

### printError - protected

Print error message to console.

```php
self::printError($message);
```

---

## Load file data

### csv - protected

Retrieve csv data.

```php
$csv = self::csv($filename);
```

### lst - protected

Retrieve lst data.

```php
$lst = self::lst($filename);
```

### json - protected

Retrieve json data.

```php
$json = self::json($filename);
```
