<?php

namespace Samy\PhpUnit;

use PHPUnit\Framework\TestCase;

class AbstractTestCase extends TestCase
{
    use PrintTrait;
    use LoadFileTrait;
}
