<?php

namespace Samy\PhpUnit;

class AbstractDataProvider
{
    use PrintTrait;
    use LoadFileTrait;
}
