<?php

namespace Samy\PhpUnit;

use Samy\DataProvider\DataProvider;

/**
 * Describes LoadFile trait.
 */
trait LoadFileTrait
{
    /**
     * Retrieve lst data.
     *
     * @param string $Filename The filename.
     * @return array<string>
     */
    protected static function lst(string $Filename): array
    {
        return DataProvider::lst($Filename);
    }

    /**
     * Retrieve csv data.
     *
     * @param string $Filename The filename.
     * @return array<array<string,string>>
     */
    protected static function csv(string $Filename): array
    {
        return DataProvider::csv($Filename);
    }

    /**
     * Retrieve json data.
     *
     * @param string $Filename The filename.
     * @return array<mixed>
     */
    protected static function json(string $Filename): array
    {
        return DataProvider::json($Filename);
    }
}
