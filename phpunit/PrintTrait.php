<?php

namespace Samy\PhpUnit;

/**
 * Describes Print trait.
 */
trait PrintTrait
{
    /**
     * Print out message to console.
     *
     * @param string $Message The message.
     * @return void
     */
    protected static function printOut(string $Message): void
    {
        self::printConsole(STDOUT, $Message);
    }

    /**
     * Print error message to console.
     *
     * @param string $Message The message.
     * @return void
     */
    protected static function printError(string $Message): void
    {
        self::printConsole(STDERR, $Message);
    }

    /**
     * Print message to console.
     *
     * @param resource $Stream The resource.
     * @param string $Message The message.
     * @return void
     */
    protected static function printConsole($Stream, string $Message): void
    {
        if (is_resource($Stream)) {
            fwrite($Stream, $Message);
        }
    }
}
