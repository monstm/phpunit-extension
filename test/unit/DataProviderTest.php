<?php

namespace Test\Unit;

use PHPUnit\Framework\TestCase;

class DataProviderTest extends TestCase
{
    /**
     * Test csv.
     *
     * @dataProvider \Test\Unit\DataProviderDataProvider::dataCsv
     * @param array<mixed> $Expect
     * @param array<mixed> $Actual
     * @return void
     */
    public function testCsv($Expect, $Actual): void
    {
        $this->assertSame($Expect, $Actual);
    }

    /**
     * Test lst.
     *
     * @dataProvider \Test\Unit\DataProviderDataProvider::dataLst
     * @param array<mixed> $Expect
     * @param array<mixed> $Actual
     * @return void
     */
    public function testLst($Expect, $Actual): void
    {
        $this->assertSame($Expect, $Actual);
    }

    /**
     * Test json.
     *
     * @dataProvider \Test\Unit\DataProviderDataProvider::dataJson
     * @param array<mixed> $Expect
     * @param array<mixed> $Actual
     * @return void
     */
    public function testJson($Expect, $Actual): void
    {
        $this->assertSame($Expect, $Actual);
    }
}
