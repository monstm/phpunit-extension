<?php

namespace Test\Unit;

class TestCaseDataProvider
{
    /**
     * Retrieve csv data provider
     *
     * @return array<array<mixed>>
     */
    public static function dataCsv(): array
    {
        return [
            [
                [
                    [
                        "id" => "1",
                        "first-name" => "Erhard",
                        "last-name" => "Everest",
                        "age" => "19"
                    ],
                    [
                        "id" => "2",
                        "first-name" => "Benetta",
                        "last-name" => "Swatton",
                        "age" => "23"
                    ],
                    [
                        "id" => "3",
                        "first-name" => "Susan",
                        "last-name" => "Grindle",
                        "age" => "21"
                    ]
                ],
                __DIR__ . DIRECTORY_SEPARATOR . "person.csv"
            ]
        ];
    }

    /**
     * Retrieve lst data provider
     *
     * @return array<array<mixed>>
     */
    public static function dataLst(): array
    {
        return [
            [
                ["Erhard", "Benetta", "Susan"],
                __DIR__ . DIRECTORY_SEPARATOR . "person.lst"
            ]
        ];
    }

    /**
     * Retrieve json data provider
     *
     * @return array<array<mixed>>
     */
    public static function dataJson(): array
    {
        return [
            [
                [
                    "person-1" => [
                        "first-name" => "Erhard",
                        "last-name" => "Everest",
                        "age" => 19
                    ],
                    "person-2" => [
                        "first-name" => "Benetta",
                        "last-name" => "Swatton",
                        "age" => 23
                    ],
                    "person-3" => [
                        "first-name" => "Susan",
                        "last-name" => "Grindle",
                        "age" => 21
                    ]
                ],
                __DIR__ . DIRECTORY_SEPARATOR . "person.json"
            ]
        ];
    }
}
