<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;

class TestCaseTest extends AbstractTestCase
{
    /**
     * Test csv.
     *
     * @dataProvider \Test\Unit\TestCaseDataProvider::dataCsv
     * @param array<mixed> $Expect
     * @param string $Filename
     * @return void
     */
    public function testCsv($Expect, $Filename): void
    {
        $this->assertSame($Expect, $this->csv($Filename));
    }

    /**
     * Test lst.
     *
     * @dataProvider \Test\Unit\TestCaseDataProvider::dataLst
     * @param array<mixed> $Expect
     * @param string $Filename
     * @return void
     */
    public function testLst($Expect, $Filename): void
    {
        $this->assertSame($Expect, $this->lst($Filename));
    }

    /**
     * Test json.
     *
     * @dataProvider \Test\Unit\TestCaseDataProvider::dataJson
     * @param array<mixed> $Expect
     * @param string $Filename
     * @return void
     */
    public function testJson($Expect, $Filename): void
    {
        $this->assertSame($Expect, $this->json($Filename));
    }
}
